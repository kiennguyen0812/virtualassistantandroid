package com.example.vaapplication.model;

public class ModelAsr {
    private String partial;

    public ModelAsr(String partial) {
        this.partial = partial;
    }

    public String getPartial() {
        return partial;
    }

    public void setPartial(String partial) {
        this.partial = partial;
    }

}
