package com.example.vaapplication.model;

public class DataModelDialog {
    private String message;
    private boolean get_debug;
    private Object context;
    public DataModelDialog(String message, Boolean get_debug, Object context){
        this.message = message;
        this.get_debug = get_debug;
        this.context = context;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isGet_debug() {
        return get_debug;
    }

    public void setGet_debug(boolean get_debug) {
        this.get_debug = get_debug;
    }

    public Object getContext() {
        return context;
    }

    public void setContext(Object context) {
        this.context = context;
    }
}
