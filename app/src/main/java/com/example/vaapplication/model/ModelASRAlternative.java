package com.example.vaapplication.model;

import java.lang.reflect.Array;

public class ModelASRAlternative {
    private String spoken_text;
    private String text;

    public ModelASRAlternative(Array rescore,String spoken_text, String text) {
        this.text = text;
        this.spoken_text = spoken_text;
    }
    
    public String getSpoken_text() {
        return spoken_text;
    }

    public String getText() {
        return text;
    }
}
