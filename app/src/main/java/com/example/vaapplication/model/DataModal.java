package com.example.vaapplication.model;

public class DataModal {
    private String text;
    private String language_code;
    private String voice_name;
    private String generator;
    private String acoustic_model;
    private String style;
    private String output_format;

    public DataModal(String text, String language_code, String voice_name, String generator, String acoustic_model, String style, String output_format){
        this.text = text;
        this.language_code = language_code;
        this.voice_name = voice_name;
        this.generator = generator;
        this.acoustic_model = acoustic_model;
        this.style = style;
        this.output_format = output_format;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVoice_name() {
        return voice_name;
    }

    public void setVoice_name(String voice_name) {
        this.voice_name = voice_name;
    }

    public String getGenerator() {
        return generator;
    }

    public void setGenerator(String generator) {
        this.generator = generator;
    }

    public String getAcoustic_model() {
        return acoustic_model;
    }

    public void setAcoustic_model(String acoustic_model) {
        this.acoustic_model = acoustic_model;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getOutput_format() {
        return output_format;
    }

    public void setOutput_format(String output_format) {
        this.output_format = output_format;
    }
    public String getLanguage_code() {
        return language_code;
    }

    public void setLanguage_code(String language_code) {
        this.language_code = language_code;
    }





}
