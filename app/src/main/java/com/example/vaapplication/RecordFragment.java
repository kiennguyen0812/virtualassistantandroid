package com.example.vaapplication;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.vaapplication.model.DataModal;
import com.example.vaapplication.model.DataModelDialog;
import com.example.vaapplication.model.ModelASRAlternative;
import com.example.vaapplication.model.ModelAsr;
import com.example.vaapplication.utils.PlayAudioManager;
import com.example.vaapplication.utils.RetrofitAPI;
import com.example.vaapplication.utils.WssConnection;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;


import ai.picovoice.cobra.Cobra;
import ai.picovoice.cobra.CobraActivationException;
import ai.picovoice.cobra.CobraActivationLimitException;
import ai.picovoice.cobra.CobraActivationRefusedException;
import ai.picovoice.cobra.CobraActivationThrottledException;
import ai.picovoice.cobra.CobraException;
import ai.picovoice.cobra.CobraInvalidArgumentException;
import kotlin.jvm.internal.Intrinsics;
import okio.ByteString;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ai.picovoice.porcupine.*;

public class RecordFragment extends Fragment implements View.OnClickListener {


    private NavController navController;

    private ImageButton listBtn;
    private ImageButton recordBtn;
    private Chronometer timer;
    private TextView textInputAsr;
    private boolean isRecording = false;

    private String recordPermission = Manifest.permission.RECORD_AUDIO;
    private int PERMISSION_CODE = 21;

    private MediaRecorder mediaRecorder;
    private String recordFile;

    //Record audio in byte
    private int SAMPLE_RATE = 16000;
    private int CHANNEL_CONFIG = AudioFormat.CHANNEL_IN_MONO;
    private int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    private int AUDIO_SOURCE = MediaRecorder.AudioSource.MIC;

    //module kotlin
    private int BufferElements2Rec = 4096;
    private int BytesPerElement = 2;// 2 bytes in 16bit format

    private AudioRecord mRec;
    private boolean status = true;

    private WssConnection connect = new WssConnection();

    //Wake-up Module
    private PorcupineManager porcupineManager = null;

    //VAD
    public Cobra cobra;
    private float voiceProbability;

    public String partialResponse;
    public Thread thread;
    public String isQuestion = "false";

    public MediaPlayer mediaPlayer = new MediaPlayer();

    public RecordFragment() {
        // Required empty public constructor
    }

    public interface saveCallBacks {
        void onSaveDialog(String dialog);
        void onSaveParagraph(String paragraph);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            cobra = new Cobra(Keys.ACCESS_KEY_PICO);
        } catch (CobraInvalidArgumentException e) {
            Log.v("Cobra", "AccessKey '%s' is invalid"+Keys.ACCESS_KEY_PICO);
        } catch (CobraActivationException e) {
            Log.v("Cobra", "AccessKey activation error");
        } catch (CobraActivationLimitException e) {
            Log.v("Cobra", "AccessKey reached its device limit");
        } catch (CobraActivationRefusedException e) {
            Log.v("Cobra", "AccessKey refused");
        } catch (CobraActivationThrottledException e) {
            Log.v("Cobra", "AccessKey has been throttled");
        } catch (CobraException e) {
            Log.v("Cobra", "Failed to initialize Cobra " + e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cobra.delete();
        stopPorcupine();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_record, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        recordBtn = view.findViewById(R.id.record_btn);
        timer = view.findViewById(R.id.record_timer);
        textInputAsr = view.findViewById(R.id.textInputAsr);
        recordBtn.setOnClickListener(this);


        //Enable WakeUp module
        if (checkPermissions()) {
            startPorcupine();
        }

        //Enable VAD module
//        if (checkPermissions()) {
//            microphoneReader.start();
//        }

    }
    public static final ModelAsr jsonToModel(String value) {

        try {
            Gson gson = new Gson();
            return gson.fromJson(value, ModelAsr.class);
        } catch (Exception e) {
            Log.v(TAG, "Exception when converting gson:"+e);
            return null;
        }
    }
    public static final ModelASRAlternative jsonToModelAlternative(String value) {

        try {
            Gson gson = new Gson();
            return gson.fromJson(value, ModelASRAlternative.class);
        } catch (Exception e) {
            Log.v(TAG, "Exception when converting gson:"+e);
            return null;
        }
    }

    public void triggerAllApi(String messsageToDialog){
        recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.record_btn_stopped, null));
        postDataDialog(messsageToDialog, true, null, new saveCallBacks() {
            @Override
            public void onSaveDialog(String dialog) {
                Log.v(TAG, "dialog response here" + dialog);
                postDataSpeech(dialog, "vi_vn", "female_south2", "melgan", "fastspeech2", "news", "mp3", new saveCallBacks() {
                    @Override
                    public void onSaveDialog(String dialog) {
                    }
                    @Override
                    public void onSaveParagraph(String paragraph) {
                        String url = Keys.urlTTS;
                        String url_music = url + paragraph;
                        try {
//                            PlayAudioManager.playAudio(url_music);
                            playAudio(url_music);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            @Override
            public void onSaveParagraph(String paragraph) {
            }
        });

//        triggerAutoRecall();
        thread.interrupt();
//        textInputAsr.setText(null);
        timer.stop();
        triggerAutoRecall();
    }

    @Override
    public void onClick(View view) {
        if (isRecording) {
            //Stop Recording
            try {
                stopRecording();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Change button image and set Recording state to false
            recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.record_btn_stopped, null));
            isRecording = false;
        } else {
            //Check permission to record audio
            if (checkPermissions()) {
                //Start Recording
                try {
                    startRecording();
                    // Change button image and set Recording state to false
                    recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.record_btn_recording, null));
                    isRecording = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean checkPermissions() {
        //Check permission
        if (ActivityCompat.checkSelfPermission(getContext(), recordPermission) == PackageManager.PERMISSION_GRANTED) {
            //Permission Granted
            return true;
        } else {
            //Permission not granted, ask for permission
            ActivityCompat.requestPermissions(getActivity(), new String[]{recordPermission}, PERMISSION_CODE);
            return false;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isRecording) {
            try {
                stopRecording();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void stopRecording() throws IOException {
        timer.stop();
        status = false;
        connect.closeSocket();
    }

    private void startRecording() throws IOException, CobraException {
        timer.setBase(SystemClock.elapsedRealtime());
        timer.start();
        status = true;

        //Trigger Record when button is pressed
        RecordAudioInByte();

        //Trigger when WakeUp Word was detected
//        RecordByteAudio();


    }

    private void postDataSpeech(String text, String language_code, String voice_name, String generator, String acoustic_model, String style, String output_format, final saveCallBacks saveCallBacks) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://dev.vinbase.ai/api/v1/tts/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // below line is to create an instance for our retrofit api class.
        RetrofitAPI retrofitAPI = retrofit.create(RetrofitAPI.class);
        // passing data from our text fields to our modal class.
        DataModal modal = new DataModal(text, language_code, voice_name, generator, acoustic_model, style, output_format);
        // calling a method to create a post and passing our modal class.
        Call<JsonObject> call = retrofitAPI.createPost(modal);
        // on below line we are executing our method.
        final String[] paragraph2 = new String[1];
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject responseFromAPI = response.body();
                //LOG
                JsonObject response_data = responseFromAPI.getAsJsonObject("data");
                paragraph2[0] = response_data.get("paragraph").getAsString();
                saveCallBacks.onSaveParagraph(paragraph2[0]);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v(TAG, "Connection Error Speech");
            }
        });
    }

    private void postDataDialog(String message, Boolean get_debug, Object context, final saveCallBacks saveCallBacks) {
        Long tsLong = System.currentTimeMillis();
        String timeStamp = tsLong.toString();
        Log.v(TAG, "CHECK URL:"+ Keys.urlDialog+timeStamp+"/");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Keys.urlDialog+timeStamp+"/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // below line is to create an instance for our retrofit api class.
        RetrofitAPI retrofitAPI = retrofit.create(RetrofitAPI.class);

        // passing data from our text fields to our modal class.
        DataModelDialog modalDialog = new DataModelDialog(message, get_debug, context);

        // calling a method to create a post and passing our modal class.
        Call<JsonObject> call = retrofitAPI.createPostDialog(modalDialog);
        // on below line we are executing our method.
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject responseFromAPI = response.body();
                Log.v(TAG, "response dialog initial "+responseFromAPI);
                String responseString = "Response Code : " + response.code();
                //LOG
                String final_text = "";
                if(response.code() == 200){
                    JsonObject response_data = responseFromAPI.getAsJsonObject("data");
                    Log.v(TAG, "response dialog data "+response_data);
                    JsonArray response_bot_message = response_data.getAsJsonArray("bot_message");
                    int index = 1;
                    JsonElement response_text;
                    if(index >= response_bot_message.size()){
                        response_text = response_bot_message.get(0);
                    }else{
                        response_text = response_bot_message.get(index);
                    }
                    JsonObject final_object = response_text.getAsJsonObject();
                    final_text = final_object.get("value").getAsString();
                    isQuestion = checkQuestion(response);
                    Log.v(TAG, "IS QUESTION " + isQuestion);

                }
                else{
                    final_text = "Vui lòng nhắc lại tôi không nghe rõ";
                }
                if (saveCallBacks != null) {
                    saveCallBacks.onSaveDialog(final_text);
                }

            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v(TAG, "Connection Error Dialog ");
            }
        });
    }
    public String checkQuestion(Response<JsonObject> response){
        JsonObject responseFromAPI = response.body();
        JsonObject response_data = responseFromAPI.getAsJsonObject("data");
        JsonObject dialog_response = response_data.getAsJsonObject("dialog_response");
        JsonObject debug_data = dialog_response.getAsJsonObject("debug_data");
        JsonPrimitive hasMissing = debug_data.getAsJsonPrimitive("hasMissing");
        String isQuestion = hasMissing.getAsString();
        return isQuestion;

    }


    private void RecordAudioInByte() {
        recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.record_btn_recording, null));
        playNotiSound();
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    short[] buffer = new short[BufferElements2Rec];
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    connect.run();
                    mRec = new AudioRecord(AUDIO_SOURCE, SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT,  BufferElements2Rec*BytesPerElement*128);
                   // WssConnection connect = new WssConnection();
                    boolean waitOpenWebsocket = true;
                    while (waitOpenWebsocket == true){
                        if(connect.checkOpen()){
                            mRec.startRecording();
                            waitOpenWebsocket = false;
                        }
                    }
                    Log.v(TAG, "CheckOpen "+connect.checkOpen());
                    Log.v(TAG, "CheckStopWSS "+connect.getStopStatus());
                    while (connect.getStopStatus() != true) {
                        mRec.read(buffer, 0, buffer.length);
                        byte[] byteAudio = shortToByte(buffer);
                        connect.sendBuffer(ByteString.of(byteAudio));
                        setTextInputAsr(connect.getTempPartialResponse());
                    }
                    partialResponse = connect.getPartialResponse();
                    Log.v(TAG, "Check response here:"+partialResponse);
//                    textInputAsr.setVisibility(TextView.VISIBLE);
                    setTextInputAsr(partialResponse);
                    connect.resumeStatus();
                    triggerAllApi(partialResponse);
//                    isRecording = false;
////                    connect.resumeStatus();
//                    Log.v(TAG, "CheckStopWSS after resume:"+connect.getStopStatus());
//                    connect.closeSocket();
//                    textInputAsr.setVisibility(TextView.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("VS", "IOException");
                }
            }
        });
        thread.start();
    }


    //Start WakeUp Module
    private void startPorcupine() {
        try {
            porcupineManager = new PorcupineManager.Builder()
                    .setAccessKey(Keys.ACCESS_KEY_PICO)
                    .setKeyword(Porcupine.BuiltInKeyword.HEY_SIRI)
                    .setSensitivity(0.5f)
                    .build(getContext(), porcupineManagerCallback);
            porcupineManager.start();
        } catch (PorcupineInvalidArgumentException e) {
            Log.v("Porcupine", "%s\nEnsure your accessKey '%s' is a valid access key." + e.getMessage());
        } catch (PorcupineActivationException e) {
            Log.v("Porcupine", "AccessKey activation error" + e.getMessage());

        } catch (PorcupineActivationLimitException e) {
            Log.v("Porcupine", "AccessKey reached its device limit" + e.getMessage());

        } catch (PorcupineActivationRefusedException e) {
            Log.v("Porcupine", "AccessKey refused" + e.getMessage());

        } catch (PorcupineActivationThrottledException e) {
            Log.v("Porcupine", "AccessKey has been throttled" + e.getMessage());

        } catch (PorcupineException e) {
            Log.v("Porcupine", "Failed to initialize Porcupine." + e.getMessage());

        }
    }

    //Stop WakeUp module
    private void stopPorcupine() {
        if (porcupineManager != null) {
            try {
                porcupineManager.stop();
                porcupineManager.delete();
            } catch (PorcupineException e) {
                Log.v("Porcupine", "Failed to initialize Porcupine." + e.getMessage());
            }
        }
    }

    //Manager WakeUp module
    private final PorcupineManagerCallback porcupineManagerCallback = new PorcupineManagerCallback() {
        @Override
        public void invoke(int keywordIndex) {
            try {
                startRecording();
            } catch (IOException | CobraException e) {
                e.printStackTrace();
            }
        }
    };

    //Function Trigger VAD module
    private void RecordByteAudio() throws CobraException {
        playNotiSound();
        Log.v(TAG, "status:"+status);
        recordBtn.setImageDrawable(getResources().getDrawable(R.drawable.record_btn_recording, null));
        thread = new Thread(new Runnable() {
           @Override
           public void run() {
               final int minBufferSize = AudioRecord.getMinBufferSize(
                       cobra.getSampleRate(),
                       AudioFormat.CHANNEL_IN_MONO,
                       AudioFormat.ENCODING_PCM_16BIT);
               final int bufferSize = Math.max(cobra.getSampleRate() / 2, minBufferSize);
               AudioRecord audioRecord = null;

               short[] buffer = new short[cobra.getFrameLength()];

               try {
                   if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                       Log.v(TAG, "PLEASE ALLOW RECORD AUDIO");
                       return;
                   }
                   audioRecord = new AudioRecord(
                           MediaRecorder.AudioSource.MIC,
                           cobra.getSampleRate(),
                           AudioFormat.CHANNEL_IN_MONO,
                           AudioFormat.ENCODING_PCM_16BIT,
                           bufferSize);
                   audioRecord.startRecording();
                   connect.run();
                   while(status==true){
                       if (audioRecord.read(buffer, 0, buffer.length) == buffer.length) {
                           voiceProbability = cobra.process(buffer);
                           Log.v(TAG, "Voice Probability is: "+voiceProbability);

                           final Handler handler = new Handler(Looper.getMainLooper());
                           handler.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   if(voiceProbability >= 0.01) {
                                       handler.postDelayed(this, 5000);
                                   }
                                   else {
                                       // do actions
                                       Log.v(TAG, "VOICE IS OVER");
                                       status = false;
                                       timer.stop();
                                   }
                               }
                           }, 3000);
                           byte[] byteAudio = shortToByte(buffer);
                           connect.sendBuffer(ByteString.of(byteAudio));
                       }
                   }
                   audioRecord.stop();
                   ModelAsr modelAsr = jsonToModel(connect.getPartialResponse());
//                   partialResponse = modelAsr.getPartial();
                   partialResponse = modelAsr.getPartial();
                   Log.v(TAG, "Partial response:"+connect.getPartialResponse());

                   if(partialResponse==null){
                       ModelASRAlternative modelASRAlternative = jsonToModelAlternative(connect.getPartialResponse());
                       partialResponse = modelASRAlternative.getText();
                   }
                   Log.v(TAG, "Final Partial response:"+partialResponse);
                   triggerAllApi(partialResponse);
                   status = true;
                   connect.closeSocket();
               } catch (IllegalArgumentException | IllegalStateException | CobraException | IOException e) {
                   try {
                       throw new CobraException(e);
                   } catch (CobraException cobraException) {
                       cobraException.printStackTrace();
                   }
               } finally {
                   if (audioRecord != null) {
                       audioRecord.release();
                   }
               }
           }
       });
       thread.start();
    }
    public final byte[] shortToByte(@NotNull short[] sData) {
        Intrinsics.checkNotNullParameter(sData, "sData");
        int shortArrSize = sData.length;
        byte[] bytes = new byte[shortArrSize * 2];
        int i = 0;

        for(int var5 = shortArrSize; i < var5; ++i) {
            bytes[i * 2] = (byte)(sData[i] & 255);
            bytes[i * 2 + 1] = (byte)(sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }
    public void playNotiSound(){
        MediaPlayer music = MediaPlayer.create(getActivity(), R.raw.sound);
        music.start();
        music.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                music.stop();
                music.release();
            }
        });
    }
    public void triggerAutoRecall(){
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.v(TAG, "Audio has completely played");
                killMediaPlayer();
                isRecording = false;
                connect.closeSocket();
                if(isQuestion == "true"){
                    Log.v(TAG, "IS QUESTION.CONTINUE ASKING");
                    timer.start();
                    isRecording = true;
                    RecordAudioInByte();
                }
            }
        });
    }

    public void setTextInputAsr(String textInput) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textInputAsr.setText(textInput);
            }
        });
    }
    public void playAudio(final String url) throws Exception {
        if (mediaPlayer == null) {
            Log.v(TAG,"Initialize mediaPlayer");
            mediaPlayer = new MediaPlayer();
        }
        mediaPlayer.reset();
        mediaPlayer.setDataSource(url);
        mediaPlayer.prepare();
        mediaPlayer.start();
    }

    public void killMediaPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
