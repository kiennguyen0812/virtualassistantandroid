package com.example.vaapplication.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class SendAudio {
    public SendAudio() {
    }

    public String ConvertToByteArray(String filepath) throws IOException {
        try {
            InputStream inputStream = new FileInputStream(filepath);
            byte[] arr = getBytesFromInputStream(inputStream);
            Log.d("byte: ", "" + Arrays.toString(arr));
            return Arrays.toString(arr);
        } catch (Exception e) {
            Log.d("byte: ", "Error Convert");
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getBytesFromInputStream(InputStream is) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[0xFFFF];
        for (int len = is.read(buffer); len != -1; len = is.read(buffer)) {
            os.write(buffer, 0, len);
        }

        return os.toByteArray();
    }
}
