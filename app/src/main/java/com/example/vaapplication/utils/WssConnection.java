package com.example.vaapplication.utils;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.vaapplication.Keys;
import com.example.vaapplication.model.ModelASRAlternative;
import com.example.vaapplication.model.ModelAsr;
import com.google.gson.Gson;

import java.io.IOException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class WssConnection extends WebSocketListener {
    public static String baseUrl = "wss://dev.vinbase.ai";
    private static String fullDomain = baseUrl+"/api/v2/asr_gpu/stream/";
    private static String TOKEN_KEY = "token";
    private static String DEVICE_ID_KEY = "device_id";
    private static String SESSSION_ID_KEY = "session_id";
    private static String FORWAD_MESSSAGE_KEY = "forward_message";
    private final ExecutorService writeExecutor = Executors.newSingleThreadExecutor();

    static Long tsLong = System.currentTimeMillis();
    static String timeStamp = tsLong.toString();
    private WebSocket webSocket;
    public boolean alreadyOpen = false;
    public String partialResponse;
    public String tempPartialResponse;
    public boolean isStopping = false;

    public void run() throws IOException {
        final OkHttpClient client = new OkHttpClient();
        String token = Keys.tokenASR;
        Request request = new Request.Builder()
                .url(fullDomain)
                .addHeader(TOKEN_KEY, token)
                .addHeader(DEVICE_ID_KEY, "iddevice")
                .addHeader(SESSSION_ID_KEY, timeStamp)
                .addHeader(FORWAD_MESSSAGE_KEY, "true")
                .build();
        webSocket = client.newWebSocket(request, this);

    }
    public void sendBuffer(ByteString buffer){
        Log.v(TAG, "Send Buffer");
        webSocket.send(buffer);
    }
    public void closeSocket(){
        Log.v(TAG, "Close Socket");
        webSocket.send("EOS");
    }
    public boolean checkOpen(){
        return alreadyOpen;
    }
    public String getPartialResponse(){
        return partialResponse;
    }
    public String getTempPartialResponse(){
        return tempPartialResponse;
    }
    public boolean getStopStatus(){return isStopping;}
    public void resumeStatus(){
        this.isStopping = false;
    }
    @Override
    public void onMessage(@NonNull WebSocket webSocket, @NonNull String text) {
        super.onMessage(webSocket, text);
        Log.v(TAG, "response On Message websocket:"+webSocket);
        Log.v(TAG, "response On Message text:"+text);

        ModelAsr modelAsr = jsonToModel(text);
//        partialResponse = modelAsr.getPartial();
        tempPartialResponse = modelAsr.getPartial();
        if(modelAsr.getPartial()==null){
            isStopping = true;
            ModelASRAlternative modelASRAlternative = jsonToModelAlternative(text);
            partialResponse = modelASRAlternative.getText();
            Log.v(TAG, "On Message Partial"+partialResponse);
            Log.v(TAG, "On Message: Speaker stopped talking");
        }
    }

    @Override
    public void onFailure(@NonNull WebSocket webSocket, @NonNull Throwable t, @Nullable Response response) {
        super.onFailure(webSocket, t, response);
        Log.v(TAG,"Error on Failure Throwable: "+t);
        Log.v(TAG, "Error on Failure: " + t.getMessage());
        Log.v(TAG, "Error on Failure response: " + response );


    }

    @Override
    public void onOpen(@NonNull WebSocket webSocket, @NonNull Response response) {
        super.onOpen(webSocket, response);
        Log.v(TAG, "Open Websocket");
        Log.v(TAG, "Response open "+response);
        alreadyOpen = true;
    }

    @Override
    public void onClosing(@NonNull WebSocket webSocket, int code, @NonNull String reason) {
        super.onClosing(webSocket, code, reason);
        Log.v(TAG, "Reason closing"+reason+" on code "+code);
        webSocket.close(1000, null);

    }

    @Override
    public void onClosed(@NonNull WebSocket webSocket, int code, @NonNull String reason) {
        super.onClosed(webSocket, code, reason);
        Log.v(TAG, "Closed websocket"+reason+" on code "+code);

    }

    public static final ModelAsr jsonToModel(String value) {

        try {
            Gson gson = new Gson();
            return gson.fromJson(value, ModelAsr.class);
        } catch (Exception e) {
            Log.v(TAG, "Exception when converting gson:"+e);
            return null;
        }
    }
    public static final ModelASRAlternative jsonToModelAlternative(String value) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(value, ModelASRAlternative.class);
        } catch (Exception e) {
            Log.v(TAG, "Exception when converting gson:"+e);
            return null;
        }

    }

}

